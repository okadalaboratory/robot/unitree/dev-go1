# Unitree Go1

## unitreeのrosラッパーをインストールする
```
$ mkdir -p unitree_ws/src
$ cd unitree_ws/src && git clone https://github.com/arata/trcp_go1.git
$ cd unitree_ws/src && catkin_make
```

## LIDARのセットアップ RoboSense RS-Helios-16P
[ここ](https://github.com/RoboSense-LiDAR/rslidar_sdk)を参考にlidarのrosラッパーをインストールする．

## rs_to_velodyneのインストールをする
[ここ](https://github.com/HViktorTsoi/rs_to_velodyne)を参考にインストールする．

## パソコンのipアドレスを192.168.123.102に設定する
ライダーがこのIPに向かってデータをブロードキャストするため

以下で実行する
```
$ roslaunch trcp_unitree bringup.launch
```


